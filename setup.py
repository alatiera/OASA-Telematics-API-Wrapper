from setuptools import setup, find_packages


with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

setup(
    name='telematics',
    description='Unofficial API wrapper of telematics.oasa.gr',
    author='Jordan Petridis',
    author_email='jordanpetridis@protonmail.com',
    url='https://github.com/alatiera/OASA-Telematics-API-Wrapper',
    version='0.0.1',
    long_description=README,
    license=LICENSE,
    install_requires=['requests'],
    packages=find_packages(exclude=['tests']),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
    ]

)
