from typing import Dict, List, NamedTuple

from . import raw_api


class Line(NamedTuple):
    """Map of the json returned from webGetLines."""

    line_code: int
    line_id: str
    line_descr: str
    line_descr_eng: str


class LineWithML(NamedTuple):
    """Map of the json returned from webGetLinesWithMLInfo."""

    line_code: int
    line_id: str
    line_descr: str
    line_descr_eng: str
    ml_code: int
    sdc_code: int


class Route(NamedTuple):
    """Map of the json returned from webGetRoutes."""

    route_code: int
    line_code: int
    descr: str
    descr_eng: str
    rtype: int
    distance: float


class RouteDetails(NamedTuple):
    """Map of the json returned from webRouteDetails."""

    # routed_long or routed_x
    long: float

    # routed_lat or routed_y
    lat: float

    order: int


class Stop(NamedTuple):
    """Map of the json returned from webGetStops."""
    code: int
    id: int
    descr: str
    descr_eng: str
    street: str
    street_eng: str
    heading: int  # Not sure if str or int
    lat: float
    long: float
    order: int
    stype: int
    amea: int  # Not sure if str or int


class ClosestStop(NamedTuple):
    """Map of the json returned from getClosestStops."""
    code: int
    id: int
    descr: str
    descr_eng: str
    street: str
    street_eng: str
    heading: int  # Not sure if str or int
    lat: float
    long: float
    distance: float


class RoutesOfStop(NamedTuple):
    """Map of the json returned from webRoutesForStop."""

    route_code: int
    line_code: int
    route_descr: str
    route_descr_eng: str
    route_type: int
    route_distance: float
    line_id: str
    line_descr: str
    line_descr_eng: str
    master_line_code: int


class RouteDetailsAndStops(NamedTuple):
    """Map of the json returned from webGetRoutesDetailsAndStops."""

    details: List[RouteDetails]
    stops: List[Stop]


class StopArrivals(NamedTuple):
    """Map of the json returned from getStopArrivals."""

    route_code: int
    veh_code: int
    btime2: int


class BusLocation(NamedTuple):
    """Map of the json returned from getBusLocation."""

    veh_no: int
    cs_date: str
    cs_lat: float
    cs_long: float
    route_code: int


class Schedule(NamedTuple):
    """Map of the json returned from getScheduleDaysMasterline."""

    descr: str
    descr_eng: str
    code: int
    # have no idea why the dict has a None string as key, ("": "0")
    none: int


class RoutesAndLinesOfML(NamedTuple):
    """Map of the json returned from getRoutesAndLinesForML."""

    line_code: int
    line_id: str
    line_descr: str
    line_descr_eng: str
    afetiria: str
    terma: str
    afetiria_en: str
    terma_en: str
    line_id_gr: str
    sdc_code: int


class RouteOfLine(NamedTuple):
    """Map of the json returned from getRoutesForLine."""

    code: int
    id: str
    descr: str
    active: int  # acts like bool
    descr_eng: str


class Name(NamedTuple):
    """Map of the json returned from getLineName, getMLName, getRouteName."""

    descr: str
    descr_eng: str


class StopName(NamedTuple):
    """Map of the json returned from getStopNameAndXY."""

    descr: str
    descr_eng: str
    lat: float
    long: float
    heading: int
    id: int


class SchedLine(NamedTuple):
    """Map of the come and go childs returned from getSchedLines."""

    line_id: str
    sde_code: int
    sdc_code: int
    sds_code: int
    sde_aa: int
    sde_line1: int
    sde_kp1: int
    sde_start1: str
    sde_end1: str
    sde_line2: int
    sde_kp2: int
    sde_start2: str
    sde_end2: str
    sde_sort: int
    sde_descr2: str
    line_circle: int
    line_descr: str
    line_descr_eng: str


class DaysLines(NamedTuple):
    """Map of the json returned from getSchedLines."""
    come: List[SchedLine]
    go: List[SchedLine]


def webGetLines() -> List[Line]:
    """Return a List of Lines NamedTuples."""
    retlist = []

    for line in raw_api.webGetLines():

        # Extract the data from the returned json
        linecode = int(line.get('LineCode'))
        lineid = str(line.get('LineID'))
        descr = str(line.get('LineDescr'))
        descreng = str(line.get('LineDescrEng'))

        # Create a NamedTuple and append it.
        retlist.append(Line(line_code=linecode, line_id=lineid,
                            line_descr=descr, line_descr_eng=descreng))

    return retlist


def webGetLinesWithMLInfo() -> List[LineWithML]:
    """Return a List of lines_with_ml NamedTuples."""
    retlist = []

    for line in raw_api.webGetLinesWithMLInfo():

        # Extract the data from the returned json
        linecode = int(line.get('line_code'))
        lineid = str(line.get('line_id'))
        descr = str(line.get('line_descr'))
        descreng = str(line.get('line_descr_eng'))
        mlcode = int(line.get('ml_code'))
        sdccode = int(line.get('sdc_code'))

        # Create a NamedTuple and append it.
        retlist.append(LineWithML(line_code=linecode, line_id=lineid,
                                  line_descr=descr, line_descr_eng=descreng,
                                  ml_code=mlcode, sdc_code=sdccode))
    return retlist


def webGetRoutes(linecode: int) -> List[Route]:
    """Return a list of Route NamedTuples."""
    retlist = []

    for route in raw_api.webGetRoutes(linecode):

        # Extract the data from the returned json
        routecode = int(route.get('RouteCode'))
        linecode = int(route.get('LineCode'))
        desc = str(route.get('RouteDescr'))
        desceng = str(route.get('RouteDescrEng'))
        rtype = int(route.get('RouteType'))
        dist = float(route.get('RouteDistance'))

        # Create a NamedTuple and append it.
        retlist.append(Route(route_code=routecode, line_code=linecode,
                             descr=desc, descr_eng=desceng,
                             rtype=rtype, distance=dist))

    return retlist


def webRouteDetails(routecode: int) -> List[RouteDetails]:
    """Return a list of RouteDetails NamedTuples."""
    retlist = []

    for route in raw_api.webRouteDetails(routecode):

        foobar = _route_details_extract(route)

        # Create a NamedTuple and append it.
        retlist.append(foobar)

    return retlist


def _route_details_extract(json_route: Dict) -> RouteDetails:
    # Extract the data from the returned json
    lng = float(json_route.get('routed_x'))
    lat = float(json_route.get('routed_y'))
    order = int(json_route.get('routed_order'))

    # Create a NamedTuple
    route = RouteDetails(long=lng, lat=lat, order=order)

    return route


def webGetStops(routecode: int) -> List[Stop]:
    """Return a list of Stop NamedTuples."""
    retlist = []

    for stop in raw_api.webGetStops(routecode):

        foobar = _stop_extract(stop)

        # Create a NamedTuple and append it.
        retlist.append(foobar)

    return retlist


def _stop_extract(stop: Dict) -> Stop:
    # Extract the data from the returned json

    code = int(stop.get('StopCode'))
    stopid = int(stop.get('StopID'))
    descr = str(stop.get('StopDescr'))
    descr_eng = str(stop.get('StopDescrEng'))
    street = str(stop.get('StopStreet'))
    street_eng = str(stop.get('StopStreetEng'))
    heading = stop.get('StopHeading')

    if heading:
        heading = int(heading)

    lat = float(stop.get('StopLat'))
    long = float(stop.get('StopLng'))
    order = int(stop.get('RouteStopOrder'))
    stype = int(stop.get('StopType'))
    amea = int(stop.get('StopAmea'))

    # Create a NamedTuple
    foobar = Stop(code=code, id=stopid, descr=descr, street=street,
                  descr_eng=descr_eng, street_eng=street_eng,
                  heading=heading, long=long, lat=lat, order=order,
                  stype=stype, amea=amea)

    return foobar


def webRoutesForStop(stopcode: int) -> List[RoutesOfStop]:
    """Return a list of RoutesOfStop NamedTuples."""
    retlist = []

    for route in raw_api.webRoutesForStop(stopcode):

        # Extract the data from the returned json
        route_code = int(route.get('RouteCode'))
        line_code = int(route.get('LineCode'))
        route_descr = str(route.get('RouteDescr'))
        route_descr_eng = str(route.get('RouteDescrEng'))
        route_type = int(route.get('RouteType'))
        route_distance = float(route.get('RouteDistance'))
        line_id = str(route.get('LineID'))
        line_descr = str(route.get('LineDescr'))
        line_descr_eng = str(route.get('LineDescrEng'))
        master_line_code = int(route.get('MasterLineCode'))

        # Create a NamedTuple and append it.
        foobar = RoutesOfStop(route_code=route_code, line_code=line_code,
                              route_descr=route_descr, route_type=route_type,
                              route_descr_eng=route_descr_eng,
                              route_distance=route_distance, line_id=line_id,
                              line_descr=line_descr,
                              line_descr_eng=line_descr_eng,
                              master_line_code=master_line_code)

        retlist.append(foobar)

    return retlist


def webGetRoutesDetailsAndStops(routecode: int) -> RouteDetailsAndStops:
    """Return a RoutesDetailsAndStop NamedTuple."""
    json = raw_api.webGetRoutesDetailsAndStops(routecode)

    details_list = []
    stops_list = []

    # Extract the data from the returned json
    for detail in json.get('details'):
        details_list.append(_route_details_extract(detail))

    for stop in json.get('stops'):
        stops_list.append(_stop_extract(stop))

    foobar = RouteDetailsAndStops(details=details_list, stops=stops_list)
    return foobar


def getStopArrivals(stopcode: int) -> List[StopArrivals]:
    """Return a list of StopArrivals NamedTuples."""
    retlist = []

    for arrival in raw_api.getStopArrivals(stopcode):

        # Extract the data from the returned json
        route_code = int(arrival.get('route_code'))
        veh_code = int(arrival.get('veh_code'))
        btime2 = int(arrival.get('btime2'))

        # Create a NamedTuple and append it.
        retlist.append(StopArrivals(route_code=route_code,
                                    veh_code=veh_code, btime2=btime2))

    return retlist


def getBusLocation(routecode: int) -> List[BusLocation]:
    """Return a list of BusLocation NamedTuples."""
    retlist = []

    for loc in raw_api.getBusLocation(routecode):

        # Extract the data from the returned json
        veh_no = int(loc.get('VEH_NO'))
        cs_date = str(loc.get('CS_DATE'))
        cs_lat = float(loc.get('CS_LAT'))
        cs_long = float(loc.get('CS_LNG'))
        route_code = int(loc.get('ROUTE_CODE'))

        # Create a NamedTuple and append it.
        retlist.append(BusLocation(veh_no=veh_no, cs_date=cs_date,
                                   cs_lat=cs_lat, cs_long=cs_long,
                                   route_code=route_code))

    return retlist


def getScheduleDaysMasterline(linecode: int) -> List[Schedule]:
    """Return a list of Schedule NamedTuples."""
    retlist = []

    for sched in raw_api.getScheduleDaysMasterline(linecode):

        # Extract the data from the returned json
        descr = str(sched.get('sdc_descr'))
        descr_eng = str(sched.get('sdc_descr_eng'))
        code = int(sched.get('sdc_code'))
        none = int(sched.get(''))

        # Create a NamedTuple and append it.
        retlist.append(
            Schedule(descr=descr, descr_eng=descr_eng, code=code, none=none))

    return retlist


def getLinesAndRoutesForMl(mlcode: int) -> List[RoutesAndLinesOfML]:
    """Return a List of RoutesAndLinesOfML NamedTuples."""
    retlist = []

    for thing in raw_api.getLinesAndRoutesForMl(mlcode):
        line_code = int(thing.get('line_code'))
        line_id = str(thing.get('line_id'))
        line_descr = str(thing.get('line_descr'))
        line_descr_eng = str(thing.get('line_descr_eng'))
        afetiria = str(thing.get('afetiria'))
        terma = str(thing.get('terma'))
        afetiria_en = str(thing.get('afetiria_en'))
        terma_en = str(thing.get('terma_en'))
        line_id_gr = str(thing.get('line_id_gr'))
        sdc_code = int(thing.get('sdc_code'))

        foobar = RoutesAndLinesOfML(line_code=line_code, line_id=line_id,
                                    line_descr=line_descr,
                                    line_descr_eng=line_descr_eng,
                                    afetiria=afetiria, terma=terma,
                                    afetiria_en=afetiria_en,
                                    terma_en=terma_en, line_id_gr=line_id_gr,
                                    sdc_code=sdc_code)

        retlist.append(foobar)

    return retlist


def getRoutesForLine(linecode: int) -> List[RouteOfLine]:
    """Return a List of RouteOfLine NamedTuples."""
    retlist = []

    for thing in raw_api.getRoutesForLine(linecode):

        code = int(thing.get('route_code'))
        route_id = str(thing.get('route_id'))
        descr = str(thing.get('route_descr'))
        descr_eng = str(thing.get('route_descr_eng'))
        active = int(thing.get('route_active'))

        retlist.append(RouteOfLine(code=code, id=route_id, active=active,
                                   descr=descr, descr_eng=descr_eng))

    return retlist


def getMLName(mlcode: int) -> Name:
    """Return a Name NamedTuple."""
    # It only gonna have one element
    for name in raw_api.getMLName(mlcode):

        descr = str(name.get('ml_descr'))
        descr_eng = str(name.get('ml_descr_eng'))
        return Name(descr=descr, descr_eng=descr_eng)

    # Error
    return None


def getLineName(linecode: int) -> Name:
    """Return a Name NamedTuple."""
    # It only gonna have one element
    for name in raw_api.getLineName(linecode):

        descr = str(name.get('line_descr'))
        descr_eng = str(name.get('line_descr_eng'))
        return Name(descr=descr, descr_eng=descr_eng)

    # Error
    return None


def getRouteName(routecode: int) -> Name:
    """Return a Name NamedTuple."""
    # It only gonna have one element
    for name in raw_api.getRouteName(routecode):

        descr = str(name.get('route_descr'))
        descr_eng = str(name.get('route_departure_eng'))
        return Name(descr=descr, descr_eng=descr_eng)

    # Error
    return None


def getStopNameAndXY(stopcode: int) -> List[StopName]:
    """Return a list of StopName NamedTuples."""
    retlist = []

    for stop in raw_api.getStopNameAndXY(stopcode):
        descr = str(stop.get('stop_descr'))
        descr_eng = str(stop.get('stop_descr_matrix_eng'))
        lat = float(stop.get('stop_lat'))
        long = float(stop.get('stop.lng'))
        heading = int(stop.get('stop_heading'))
        stopid = int(stop.get('stop_id'))

        retlist.append(StopName(descr=descr, descr_eng=descr_eng,
                                lat=lat, long=long, heading=heading,
                                id=stopid))

    return retlist


def getSchedLines(mlcode: int, linecode: int, sdc_code: int) -> DaysLines:
    """Return a DaysLines NamedTuple."""
    req = raw_api.getSchedLines(
        mlcode=mlcode, linecode=linecode, sdc_code=sdc_code)
    come = req.get('come')
    go = req.get('go')

    comelist = []
    golist = []

    for line in come:
        comelist.append(_sched_extract(line))

    for line in go:
        golist.append(_sched_extract(line))

    return DaysLines(come=comelist, go=golist)


def _sched_extract(json: Dict) -> SchedLine:
    line_id = str(json.get('line_id'))
    sde_code = int(json.get('sde_code'))
    sdc_code = int(json.get('sdc_code'))
    sds_code = int(json.get('sds_code'))
    sde_aa = int(json.get('sde_aa'))

    sde_line1 = json.get('sde_line1')
    if sde_line1:
        sde_line1 = int(sde_line1)

    sde_kp1 = int(json.get('sde_kp1'))
    sde_start1 = str(json.get('sde_start1'))
    sde_end1 = str(json.get('sde_end1'))

    sde_line2 = json.get('sde_line2')
    if sde_line2:
        sde_line2 = int(sde_line2)

    sde_kp2 = int(json.get('sde_kp2'))
    sde_start2 = str(json.get('sde_start2'))
    sde_end2 = str(json.get('sde_end2'))
    sde_sort = int(json.get('sde_sort'))
    sde_descr2 = str(json.get('sde_descr2'))
    line_circle = int(json.get('line_circle'))
    line_descr = str(json.get('line_descr'))
    line_descr_eng = str(json.get('line_descr_eng'))

    foobar = SchedLine(line_id=line_id, sde_code=sde_code, sdc_code=sdc_code,
                       sds_code=sds_code, sde_aa=sde_aa, sde_line1=sde_line1,
                       sde_kp1=sde_kp1, sde_start1=sde_start1,
                       sde_end1=sde_end1, sde_line2=sde_line2, sde_kp2=sde_kp2,
                       sde_start2=sde_start2, sde_end2=sde_end2,
                       sde_sort=sde_sort, sde_descr2=sde_descr2,
                       line_circle=line_circle, line_descr=line_descr,
                       line_descr_eng=line_descr_eng)
    return foobar


def getClosestStops(long: float, lat: float) -> List[ClosestStop]:
    """Return a list of ClosestStop NamedTuples."""
    retlist = []

    for stop in raw_api.getClosestStops(long=long, lat=lat):

        code = int(stop.get('StopCode'))
        stopid = int(stop.get('StopID'))
        descr = str(stop.get('StopDescr'))
        descr_eng = str(stop.get('StopDescrEng'))
        street = str(stop.get('StopStreet'))
        street_eng = str(stop.get('StopStreetEng'))
        heading = stop.get('StopHeading')

        if heading:
            heading = int(heading)

        lat = float(stop.get('StopLat'))
        long = float(stop.get('StopLng'))
        distance = float(stop.get('distance'))

        # Create a NamedTuple
        foobar = ClosestStop(code=code, id=stopid, descr=descr, street=street,
                             descr_eng=descr_eng, street_eng=street_eng,
                             heading=heading, long=long, lat=lat,
                             distance=distance)
        retlist.append(foobar)

    return retlist
